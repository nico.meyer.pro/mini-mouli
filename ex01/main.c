/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/12 02:37:58 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/13 23:06:40 by pfinini          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Libs
#include <stdio.h>
#include <string.h>
#include "../print.h"

// Test functions
void	test(char *src, unsigned int n);
void	print_res(char *dest, char *ret, char *exp);

// Exercise function
char	*ft_strncpy(char *dest, char *src, unsigned int n);

// Write all tests here
int	main(void)
{
	test("42 c'est cool wsh", 4);
	test("42", 8);
}

// Write the test function
void	test(char *src, unsigned int n)
{
	char    dest[20];
	char    ft_dest[20];
	char    *exp = strncpy(dest, src, n);
	char    *ret = ft_strncpy(ft_dest, src, n);

	printf("Testing with: ");
	print_str("%s", src, YEL);
	printf(" with a size of: ");
	print_int("%d\n", n, YEL);
	print_res(ft_dest, ret, exp);
}

// Write the function that displays the result
void	print_res(char *dest, char *ret, char *exp)
{
	printf("Expecting: ");
	print_str("%s\n", exp, MAG);
	if (strcmp(exp, ret) == 0 && strcmp(exp, dest) == 0)
		print("OK\n", GRN);
	else
	{
		print_str("NOT OK, got: dest=%s ", dest, RED);
		print_str("and ret=%s\n", ret, RED);
		printf("strcmp return %d\n", strcmp(exp, ret));
	}
	printf("==============================================\n\n");
}
